#include "hw5.h"
#include "printf.h"

configuration hw5AppC{}

implementation{
  
  //Components declaration 
  components MainC, hw5C as App, RandomC;
  components new AMSenderC(AM_RADIO_MSG);
  components new AMReceiverC(AM_RADIO_MSG);
  components new TimerMilliC();
  components PrintfC;
  components SerialStartC;
  components ActiveMessageC;
  
  //Wiring the interfaces inside the app to the respective components
  App.Boot -> MainC.Boot;
  App.Receive -> AMReceiverC;
  App.AMSend -> AMSenderC;
  App.AMControl -> ActiveMessageC;
  App.LoopTimer -> TimerMilliC;
  App.Packet -> AMSenderC;
  App.Random -> RandomC;
}
