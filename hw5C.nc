#include "Timer.h"
#include "hw5.h"
#include "printf.h"

// 1. Specify the interfaces that we will use
module hw5C @safe(){
	uses {
		interface Random;
		interface Boot;
		interface Receive;
		interface AMSend;
		interface Timer<TMilli> as LoopTimer;
		interface SplitControl as AMControl;
		interface Packet;
	}
}

implementation{
	
	// 2. Define support variables
	
	// message structure pointer
	message_t msg_buff;
	// busy variable for radio resource access
	bool busy;
	
	// 3. Initialize & start the radio
	event void Boot.booted() {
		call AMControl.start();
	}
	// 4. Check if the radio turned on correctly, if not retry.
	// When the radio is on, start looper timer only for mote2 & mote3 since they are the senders
	event void AMControl.startDone(error_t err) {
		if (err == SUCCESS) {
			if (TOS_NODE_ID == 2 || TOS_NODE_ID == 3) { 
				call LoopTimer.startPeriodic(5000);
			}
			else {
				return; 
			}
		}
		else {
			call AMControl.start(); // try again to start it
		} 
	}

	// No actions to be performed when the stopping is done
	event void AMControl.stopDone(error_t err) { }
	  
	// 5. Application Logic, valid for mote 2 & mote 3
	event void LoopTimer.fired() {
		if (busy) {
			// if there is an ongoing sending don't overflow the radio
			return; 
		} 
		else {
			// allocate memory for the new message
			radio_msg_t* payload = (radio_msg_t*)call Packet.getPayload(&msg_buff, sizeof(radio_msg_t));
			// in case of error, just return
			if (payload == NULL) {
				return;
			} 
			else {
				// fill message with a random number [1,100] and the sender node identifier
				payload->rand_val =  (call Random.rand16() % 100) + 1; ;
				payload->topic = TOS_NODE_ID;
				// send the message in unicast to mote #1, the only receiver/sink
				if (call AMSend.send(1, &msg_buff, sizeof(radio_msg_t)) == SUCCESS) {
					// if the sending starts signal that the radio is in use
					busy = TRUE;

				}
			}
		}
	}
	
	// When the sending is done signal that the radio is not anymore in use
	event void AMSend.sendDone(message_t* msg, error_t error) {
		if (&msg_buff == msg) { busy = FALSE; }
	}	
	
	// 6. Message Receive, valid only for mote 1 since all the messages are sent to it
	event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
		// if the message received does not correspond to radio_message return immediately
		if (len != sizeof(radio_msg_t)) {
			return msg;
		} else {
			// cast message to radio_msg
			radio_msg_t* rcm = (radio_msg_t*)payload;
			// put into a string the value and sender id as the topic
			// and send to mote output (and TCP server in cooja) with printf
			printf("Value%uTopic%u\n", rcm->rand_val, rcm->topic);
			// flush the output buffer
			printfflush();
			return msg;
		}
	}
	
}
