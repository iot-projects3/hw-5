#ifndef HOMEWORK_5_H
#define HOMEWORK_5_H

// Structure of the message between the motes, with sender ID and value
typedef nx_struct homework5_msg {
  nx_uint16_t topic;
  nx_uint16_t rand_val;
} radio_msg_t;

// Identifier number of the message type for AMSenderC & AMReceiverC initialization.
enum {
  AM_RADIO_MSG = 6
};

#endif
